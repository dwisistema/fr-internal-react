package com.forgerockdemo;

import com.google.firebase.messaging.RemoteMessage;

import org.forgerock.android.auth.FRAClient;
import org.forgerock.android.auth.PushNotification;
import org.forgerock.android.auth.exception.InvalidNotificationException;
import org.forgerock.android.auth.Mechanism;
import org.forgerock.android.auth.exception.InvalidNotificationException;

public class FRAClientUtil {

    public static FRAClient fraClient;

    public static void setFraClient(FRAClient fraClient){
        FRAClientUtil.fraClient = fraClient;
    }

    public static PushNotification getPushNotificationFromRemoteMessage(RemoteMessage message){
        try {
            return fraClient.handleMessage(message);
        } catch (InvalidNotificationException e) {
            e.printStackTrace();
            throw new RuntimeException("Exception while occurred deserialising remote message to push notification object.");
        }
    }

     public static Mechanism getMechanism(PushNotification pushNotification) {
        return fraClient.getMechanism(pushNotification);
    }
}
