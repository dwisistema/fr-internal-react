package com.forgerockdemo;


import android.net.Uri;
import android.util.Log;
import android.widget.Button;

import androidx.annotation.NonNull;
import org.forgerock.android.auth.Node;

import org.forgerock.android.auth.FRAListener;
import org.forgerock.android.auth.PushNotification;
import org.forgerock.android.auth.exception.InvalidNotificationException;



public class PushClientUtil {
    // private Button acceptButton;
	// private Button rejectButton;


    public static PushNotification pushNotificationForRemote = null;
    private static Node currentNode;

    public static void setCurrentNode(Node node){
        currentNode = node;

    }
public static void setPushNotification(PushNotification pushNotification) {

    pushNotificationForRemote = pushNotification;
    System.out.println("setPushNotification ===>>>" + pushNotificationForRemote);

    // showAcceptReject();
}

    public static void  showAcceptReject(boolean acceptreject) {
        System.out.println("showAcceptReject ===>>>"+pushNotificationForRemote +"   ::"+ acceptreject);
        if(pushNotificationForRemote != null) {
      if(acceptreject) {
        accept();
        System.out.println("accept === ===>>>");
      } else {
        reject();
      }
    }

    }
    // public final void accept(@NonNull FRAListener<Void> listener) {
    //     if(this.pushMechanism.getAccount() != null && this.pushMechanism.getAccount().isLocked()) {
    //         listener.onException(new AccountLockException("Unable to process the Push " +
    //                 "Authentication request: Account is locked."));
    //     } else if (this.pushType == PushType.DEFAULT) {
    //         Logger.debug(TAG, "Accept Push Authentication request for message: %s", getMessageId());
    //         performAcceptDenyAsync(true, listener);
    //     } else {
    //         listener.onException(new PushMechanismException("Error processing the Push " +
    //                 "Authentication request. This method cannot be used to process notification " +
    //                 "of type: " + this.pushType));
    //     }
    // }

    private static void accept() {
        pushNotificationForRemote.accept(new FRAListener<Void>() {
            @Override
            public void onSuccess(Void result) {    
                System.out.println("Successfully logged in via push notificaiton"+result);
        
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();;
        
            }
         
        });
 
    }

    public static void reject() {
        pushNotificationForRemote.deny(new FRAListener<Void>() {
            @Override
            public void onSuccess(Void result) {
                
                System.out.println("Successfully deny in via push notificaiton"+result);
                
            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();;
            }

        });
    } 
}
