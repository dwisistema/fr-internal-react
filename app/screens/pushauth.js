import React, { useState, Component, useEffect } from 'react';
import { Text, View, StyleSheet, TouchableHighlight, NativeModules, LogBox, TextInput, Alert, Modal, TouchableOpacity, Dimensions, PermissionsAndroid } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

const { ForgeRockModule } = NativeModules;

LogBox.ignoreAllLogs();


export default function PushAuth(props) {

  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [pushNotification, setNotification] = useState('');
  const [notificationBool, setNotificationBool] = useState(false);
  const [accessTokenBool, setAccessTokenBool] = useState(false);
  const [accessObj, setAccessObj] = useState({});
  const [isDeviceRegistered, setDeviceRegistered] = useState(false);
  let showNext = true;
  let intervalId;

  useEffect(() => {
    console.log("Check notification permission...")
    requestNotificationPermission()
  }, [])

  const requestNotificationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.POST_NOTIFICATIONS,
        {
          title: 'ForgeRock Demo App Notification Permission',
          message:
            'ForgeRock Demo App needs allowing notifications ' +
            'so you can use push auth feature.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Reject',
          buttonPositive: 'Allowed',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can allowed notifications');
      } else {
        console.log('Notification permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  /**
   * Run [getData] method each two seconds, until interval is cleared.
   * 
   * @see getData
   */
  useEffect(() => {

    intervalId = setInterval(() => {
      getData()
    }, 2000);

    return () => clearInterval(intervalId);
  }, []);

  /**
   * Get data accessToken to waiting response from push auth service.
   * It will run each two seconds until accessTokenResponse not empty object.
   * 
   * @see ForgeRockModule.getAccessToken
   */
  const getData = async () => {
    console.log('Waiting for response ===>>>');
    let accessTokenResponse = {};
    accessTokenResponse = await ForgeRockModule.getAccessToken();
    console.log('accessTokenResponse =>>', accessTokenResponse);
    if (accessTokenResponse != {}) {
      clearInterval(intervalId)
      setAccessObj(accessTokenResponse);
      setAccessTokenBool(true);
    }
  }

  /**
   * Run service [MFA_PushAuth].
   * And setup result for registered new device and notification response.
   * 
   * @param bool is registered device
   * @see ForgeRockModule.pushAuthLogin
   */
  const onPress = (bool) => {
    if (name != '' && password != '') {
      ForgeRockModule.pushAuthLogin(name, password, bool, (e) => {
        console.log('get notification ==>>', e);
        // getData();
        if (e === 'newDevice') {
          setDeviceRegistered(true);
        } else {
          setNotificationBool(true);
          setNotification(e);
        }

      });
    }
  };

  return (
    <View style={styles.container}>

      {/* Field Username */}
      <TextInput
        style={{
          height: 40,
          margin: 12,
          borderWidth: 1,
          padding: 10,
          width: 300
        }}
        onChangeText={(t) => {
          setName(t);
        }}
        value={name}
        placeholder="name placeholder"
      />

      {/* Field Password */}
      <TextInput
        style={{
          height: 40,
          margin: 12,
          borderWidth: 1,
          padding: 10,
          width: 300
        }}
        onChangeText={(t) => {
          setPassword(t);
        }}
        value={password}
        placeholder="password placeholder"
      />

      {/* Button Next, only show if showNext state is true */}
      {showNext ? (
        <TouchableHighlight style={styles.button} onPress={() => { onPress(false) }} underlayColor='#99d9f4'>
          <Text style={styles.buttonText}>Next</Text>
        </TouchableHighlight>
      ) : null}

      {/* Modal to show the accessToken from notification result */}
      <Modal
        visible={accessTokenBool}
        animationType="none"
        transparent
        onRequestClose={this._cancel}>
        <View style={{ backgroundColor: 'white', height: Dimensions.get('window').height - 50, width: Dimensions.get('window').width, marginTop: 10, marginLeft: 10, borderRadius: 20, justifyContent: 'center', alignItems: 'center', padding: 10 }}>
          <ScrollView>
            <Text style={{ color: "black" }} >
              {accessObj?.accessToken}
            </Text>
          </ScrollView>
          <TouchableOpacity onPress={() => {
            clearInterval(intervalId)
            setAccessTokenBool(false);
          }} style={{ backgroundColor: 'skyblue', height: 40, width: 40, justifyContent: 'center', alignItems: 'center', borderRadius: 20, position: 'absolute', right: 20, top: 10 }}>
            <Text style={{ color: 'black', fontSize: 20, fontWeight: '900' }} >X</Text>
          </TouchableOpacity>
        </View>
      </Modal>

      {
        /* 
        * Modal to show alert accept or reject push auth.
        * see [ForgeRockModule.acceptRejectCall] to handle response.
        */
      }
      <Modal
        visible={notificationBool}
        animationType="none"
        transparent
        onRequestClose={this._cancel}>
        <View style={{ backgroundColor: 'skyblue', width: 300, marginTop: '60%', marginLeft: "10%", borderRadius: 20, justifyContent: 'center', alignItems: 'center', padding: 20 }}>
          <Text style={{}} >
            Please provide your choice
          </Text>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%', padding: 20 }}>
            <TouchableOpacity onPress={() => {
              setNotificationBool(false)
              console.log('btn PushNotificationMessage ==>>', pushNotification);
              ForgeRockModule.acceptRejectCall(true, pushNotification)
            }} style={{ padding: 10, backgroundColor: 'green', borderRadius: 10 }}>
              <Text style={{ color: 'white' }}>Accept </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {
              setNotificationBool(false)
              console.log('btn PushNotificationMessage ==>>', pushNotification);
              ForgeRockModule.acceptRejectCall(false, pushNotification)
            }} style={{ padding: 10, backgroundColor: 'red', borderRadius: 10 }}>
              <Text style={{ color: 'white' }}>Reject </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>

      {
        /* 
        * Currently, this modal is not used because will throw error: 
        * java.lang.RuntimeException: Illegal callback invocation from native module. This callback type only permits a single invocation from native code. 
        */
      }
      <Modal
        visible={isDeviceRegistered}
        animationType="none"
        transparent
        onRequestClose={this._cancel}>
        <View style={{ backgroundColor: 'skyblue', width: 300, marginTop: '60%', marginLeft: "10%", borderRadius: 20, justifyContent: 'center', alignItems: 'center', padding: 20 }}>
          <Text style={{}} >
            You Want to Register Your Device
          </Text>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%', padding: 20 }}>
            <TouchableOpacity onPress={() => {
              setDeviceRegistered(false)
              onPress(true)
            }} style={{ padding: 10, backgroundColor: 'green', borderRadius: 10 }}>
              <Text style={{ color: 'white' }}>Accept </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {
              setDeviceRegistered(false)
            }} style={{ padding: 10, backgroundColor: 'red', borderRadius: 10 }}>
              <Text style={{ color: 'white' }}>Reject </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View> // End container
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    marginTop: 50,
    padding: 20,
    backgroundColor: '#ffffff',
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  button: {
    height: 36,
    backgroundColor: '#48BBEC',
    borderColor: '#48BBEC',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  buttonC: {
    height: 36,
    backgroundColor: '#e330d7',
    borderColor: '#e330d7',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
});