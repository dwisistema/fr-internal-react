// Homescreen.js
import React, { useEffect, useState} from 'react';
import { View, Text, NativeModules} from 'react-native';
import { Button } from 'react-native-elements';
import { proceedWithAuthenticated } from './helper';
import useTimeBlockedCallback from '../hooks/useTimeBlockedCallback'

const { ForgeRockModule } = NativeModules;


export default function Homescreen(props) {
    // Similar to componentDidMount and componentDidUpdate:
    useEffect(() => {
        if (!props.route.params) {
            console.log("xxxxxxxxxxxxxxxxxxxxxx");
            console.log("Start ForgeRock Auth");
            console.log("xxxxxxxxxxxxxxxxxxxxxx");
            
            /**
             * * Start two module [FRAuth] and [FRAClient]
             * @see ForgeRockModule.frAuthStart
             */
            ForgeRockModule.frAuthStart();

            // when auth client start and FRoption set it will redirect to login screen 
            setTimeout(()=> {
                props.navigation.navigate('Homescreen', {});
            },100)
        }
        if (props.route.params && props.route.params.errorMsg) {
            ForgeRockModule.performUserLogout();
            alert(props.route.params.errorMsg);
            props.route.params.errorMsg = "";
        }
    });

    useEffect(() => { 
        getData()
    },[]);

    /**
     * Run login with [Login] service
     * @see ForgeRockModule.performUserLoginWithoutUIWithCallback
     */
    const onLogin = useTimeBlockedCallback(() => {
        ForgeRockModule.performUserLoginWithoutUIWithCallback(async (responseArray) => {
            console.log("xxxxResponse Arrayxxxx");
            console.log(responseArray);
            console.log("xxxxxxxxxxxxxxxxxxxxxx");
            if (responseArray.error == "User is already authenticated") {
                const userInfoResponse = await ForgeRockModule.getUserInfo();
                responseArray.accessToken = userInfoResponse.accessToken;
                console.log("xxxxResponse Array with tokenxxxx");
                console.log(userInfoResponse);
                console.log("xxxxxxxxxxxxxxxxxxxxxx");
                proceedWithAuthenticated(props, responseArray, []);
            } else {
                var callbacks = [];
                if (responseArray.indexArray) { //from iOS in unordered dictionary
                    console.log("xxxxxxIndex Arrayxxxxx");
                    console.log(responseArray.indexArray);
                    console.log("xxxxxxxxxxxxxxxxxxxxxx");
                    responseArray.indexArray.indexArr.forEach(element => {
                        callbacks.push(responseArray[element]);    
                    });
                } else {
                    callbacks = Object.values(responseArray).map(item => ({ prompt: item.prompt, type: item.type, choices: item.choices, providers: item.providers, name: item.name, checked: item.checked }));
                    console.log("xxxxxxxx--callbacks---xxxxxxxx");
                    console.log(callbacks);
                }
                props.navigation.navigate('Login', callbacks);
            }
        });
    });
    
    /**
     * Run [MFA_FIDO2] service using [ForgeRockModule.authenticateWithTree]
     * @param treeName name of tree in server
     */
    const sendInputFido = useTimeBlockedCallback((treeName) => {
        ForgeRockModule.authenticateWithTree(treeName, (responseArray) => {
            console.log("xxxxResponse Arrayxxxx");
            console.log(responseArray);
            console.log("xxxxxxxxxxxxxxxxxxxxxx");
            if (responseArray.accessToken) {
                responseArray.error = "User is already authenticated";
                proceedWithAuthenticated(props, responseArray, []);               
            } else if (responseArray.error) {
                var error = JSON.parse(responseArray.error);
                alert(error.code + " :: " + error.message);          
            } else {
                var callbacks = [];
                if (responseArray.indexArray) { //from iOS in unordered dictionary
                    console.log("xxxxxxIndex Arrayxxxxx");
                    console.log(responseArray.indexArray);
                    console.log("xxxxxxxxxxxxxxxxxxxxxx");
                    responseArray.indexArray.indexArr.forEach(element => {
                        callbacks.push(responseArray[element]);    
                    });
                } else {
                    callbacks = Object.values(responseArray).map(item => ({ prompt: item.prompt, type: item.type, choices: item.choices, providers: item.providers, name: item.name, checked: item.checked }));
                }
                props.navigation.navigate('Fido', callbacks);
            }
        });
    });
    
    // if user already login it will logout call 
    const getData =  async () => {
        let  accessTokenResponse = {};
        accessTokenResponse =   await ForgeRockModule.getAccessToken();
        console.log('accessTokenResponse =>>', accessTokenResponse);
        if(accessTokenResponse != {}) {
            ForgeRockModule.performUserLogout();
        }
    }

    const onFido = () => {
        sendInputFido("MFA_FIDO2")
    }

    const onPushAuth = () => {
        props.navigation.navigate('PushAuth', {})
    }

    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Welcome to Bookeep{"\n"}</Text>
            <Button
                title="Register"
                onPress={() => props.navigation.navigate('Register', {})}
            />
            <Text>{"\n"}</Text>
            <Button
                title="Login"
                onPress={onLogin}
            />
            <Text>{"\n"}</Text>            
            <Button
                onPress={onFido}
                title="Fido2"
                color="#841584"
                accessibilityLabel="Access fido2 feature"
            />
            <Text>{"\n"}</Text>
            <Button
                onPress={onPushAuth}
                title="Push Auth"
                color="#841584"
                accessibilityLabel="Access push auth feature"
            />
        </View>
    );
}