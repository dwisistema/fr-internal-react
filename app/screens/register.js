import { useEffect, useState } from 'react';
import { Text, View, StyleSheet, TouchableHighlight, NativeModules, Modal, TouchableOpacity, TextInput, FlatList} from 'react-native';

const { ForgeRockModule } = NativeModules;

export default function Register(props) {

  const [showAlert, setShowAlert] = useState(false)
  const [errorMessage, setErrorMessage] = useState("")

  const [emailValidError, setEmailValidError] = useState('');
  const [passwordValidError, setPasswordValidError] = useState('');
  const [passwordMatchError, setPasswordMatchError] = useState('')

  const [username, setUsername] = useState("")
  const [firstName, setFirstName] = useState("")
  const [lastName, setLastName] = useState("")
  const [email, setEmail] = useState("")
  const [phone, setPhone] = useState("")
  const [password, setPassword] = useState("")
  const [confirmPassword, setConfirmPassword] = useState("")
  let intervalId;

  /**
   * Run [getData] each two seconds, until interval is cleared.
   * It will waiting response from registration process.
   * 
   * @see getData
   */
  useEffect(() => {
    intervalId = setInterval(() => {
        getData()
    }, 2000);

    return () => clearInterval(intervalId);
  }, []);

  /**
   * Get data accessToken to waiting response from registration service.
   * It will run each two seconds until accessTokenResponse not empty object.
   * 
   * @see ForgeRockModule.getAccessToken
   */
  const getData = async () => {
    console.log('Waiting for response ===>>>');
    let accessTokenResponse = {};
    accessTokenResponse = await ForgeRockModule.getAccessToken();
    console.log('accessTokenResponse =>>', accessTokenResponse);
    if (accessTokenResponse != {}) {
      clearInterval(intervalId)
      props.navigation.navigate("MyTabs")
    }
  }

  /**
   * Run [Register] service
   * @see ForgeRockModule.register
   */
  const onPress = () => {
    if (!isAllFieldReady()) { return }

    /**
     * Prepare object value to insert new register user
     * [givenName, sn, mail, phone] key must be syncronize from/to the server (dashboard)
     * 
     * TODO: Always check [givenName, sn, mail, phone] attribute, to make sure the service is matching
     */
    const userObject = {
      'givenName': firstName,
      'sn': lastName,
      'mail': email,
      'phone': phone,
      'username': username,
      'password': password,
    }
    console.log(userObject)

    ForgeRockModule.register(userObject, (s) => {
      console.log('get value exception ==>>', s);
      if (s.includes("exception")) {
        setShowAlert(true);
        setErrorMessage(s);
      } else if (s == "isnotvalidate") {
        setShowAlert(true);
        setErrorMessage("Username is already used!");
      }
    })
  };

  /**
   * Validate field is ready
   * @return [Boolean] true if ready otherwise false
   */
  const isAllFieldReady = () => {
    return username.trim() !== "" &&
          firstName.trim() !== "" &&
          lastName.trim() !== "" && 
          email.trim() !== "" &&
          phone.trim() !== "" &&
          password.trim() !== "" &&
          confirmPassword.trim() !== ""
  }

  /**
   * Check email validation
   * If email not valid, setEmailValidError message
   */
  const checkEmailValid = (email) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    if (email.length == 0) {
      setEmailValidError("")
    }else if (reg.test(email) === false) {
      setEmailValidError("Email is not valid!")
    } else {
      setEmailValidError("")
    }
  }

  /**
   * Check password validation
   * If password not valid, setPasswordValidError message
   */
  const checkPasswordValid = (password) => {
    let reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/
    if (password.length == 0) {
      setPasswordValidError("")
    } else if (password.length < 8) {
      setPasswordValidError("Must be at least 8 characters long")
    } else if ((reg.test(password) === false)) {
      setPasswordValidError("Password not valid")
    } else {
      setPasswordValidError("")
    }
  }

  /**
   * Check confirm password validation
   * @return [Boolean] true if password and confirmPassword is match, otherwise false
   */
  const checkPasswordMatch = (confirmPassword) => {
    if (password !== confirmPassword) {
      setPasswordMatchError("Password is not match!")
    } else {
      setPasswordMatchError("")
    }
  }


  return (
    <View style={styles.container}>
        <TextInput
          style={{
            height: 40,
            margin: 12,
            borderWidth: 1,
            padding: 10,
            width: 300
          }}
          onChangeText={(t) => { setUsername(t); }}
          value={username}
          placeholder="Username"
          keyboardType="default"
        />
        
        <TextInput
          style={{
            height: 40,
            margin: 12,
            borderWidth: 1,
            padding: 10,
            width: 300
          }}
          onChangeText={(t) => { setFirstName(t); }}
          value={firstName}
          placeholder="Firstname"
          keyboardType="default"
        />

        <TextInput
          style={{
            height: 40,
            margin: 12,
            borderWidth: 1,
            padding: 10,
            width: 300
          }}
          onChangeText={(t) => { setLastName(t); }}
          value={lastName}
          placeholder="Lastname"
          keyboardType="default"
        />

        <TextInput
          style={{
            height: 40,
            margin: 12,
            borderWidth: 1,
            padding: 10,
            width: 300
          }}
          onChangeText={(t) => { 
            setEmail(t) 
            checkEmailValid(t)
          }}
          value={email}
          placeholder="Email"
          keyboardType="email-address"
        />
        {emailValidError ? <Text style={{color: 'red'}}>{emailValidError}</Text> : null}

        <TextInput
          style={{
            height: 40,
            margin: 12,
            borderWidth: 1,
            padding: 10,
            width: 300
          }}
          onChangeText={(t) => { setPhone(t); }}
          value={phone}
          placeholder="Phone"
          keyboardType="phone-pad"
        />

        <TextInput
          style={{
            height: 40,
            margin: 12,
            borderWidth: 1,
            padding: 10,
            width: 300
          }}
          onChangeText={(t) => { 
            setPassword(t)
            checkPasswordValid(t)
          }}
          secureTextEntry={true}
          value={password}
          placeholder="Password"
          keyboardType="default"
        />
        <FlatList
          data={[
            {key: 'Must be at least 8 characters long'},
            {key: 'Must have at least 1 capital letter(s)'},
            {key: 'Must have at least 1 number(s)'},
          ]}
          renderItem={({item}) => <Text style={styles.item}>- {item.key}</Text>}
        />
        {passwordValidError ? <Text style={{color: 'red'}}>{passwordValidError}</Text> : null}

        <TextInput
          style={{
            height: 40,
            margin: 12,
            borderWidth: 1,
            padding: 10,
            width: 300
          }}
          onChangeText={(t) => { 
            setConfirmPassword(t)
            checkPasswordMatch(t)
          }}
          secureTextEntry={true}
          value={confirmPassword}
          placeholder="Confirm Password"
          keyboardType="default"
        />
        {passwordMatchError ? <Text style={{color: 'red'}}>{passwordMatchError}</Text> : null}

        {/* Button Register, to run service */}
        <TouchableHighlight style={styles.button} onPress={() => { onPress() }} underlayColor='#99d9f4'>
            <Text style={styles.buttonText}>Register</Text>
        </TouchableHighlight>

        {/* Modal to info user that something wrong when register process
        * //* Only show modal when showAlert state is true
        */}
        <Modal
        visible={showAlert}
        animationType="none"
        transparent
        onRequestClose={this._cancel}>
        <View style={{ backgroundColor: 'skyblue', width: 300, marginTop: '60%', marginLeft: "10%", borderRadius: 20, justifyContent: 'center', alignItems: 'center', padding: 20 }}>
          <Text style={{}} >
            Something Wrong with the message:
            {errorMessage}
          </Text>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-end', width: '100%', padding: 20 }}>
            <TouchableOpacity onPress={() => {
              setShowAlert(false)
            }} style={{ padding: 10, backgroundColor: 'green', borderRadius: 10 }}>
              <Text style={{ color: 'white' }}>OK</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>

    </View> // End container
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    marginTop: 50,
    padding: 20,
    backgroundColor: '#ffffff',
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  button: {
    height: 36,
    backgroundColor: '#48BBEC',
    borderColor: '#48BBEC',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
});